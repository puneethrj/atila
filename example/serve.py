import skitai
import os
if os.path.isfile ("example/resources/sqlite3.db"):
    os.remove ("example/resources/sqlite3.db")

# mount app with runtime preference
with skitai.preference () as pref:
    # mount app
    pref.debug = True
    pref.use_reloader = True
    skitai.mount ("/", './example/app.py', pref = pref)

    # mount static directory for file services
    skitai.mount ("/", './example/static')

# alias your upstreams
skitai.alias ("@pypi", skitai.PROTO_HTTPS, "pypi.org")
skitai.alias ("@sqlite3", skitai.DB_SQLITE3, "example/resources/sqlite3.db")

# run server
skitai.run (port = 5000)
