Starting Example
============================

.. code-block:: bash

  pip3 install -Ur requirements.txt
  python3 server.py

You can access via your browser: http://localhost:5000

