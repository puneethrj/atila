import pytest
import skitai
from functools import partial

@pytest.fixture
def launch ():
    return partial (skitai.test_client, port = 30371, silent = False)
