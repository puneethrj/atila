from atila import Atila
from services import apis
from services import templates
from services import websocket
import skitai

app = Atila (__name__)

app.mount ('/apis', apis)
app.mount ('/templates', templates)
app.mount ('/websocket', websocket)

@app.route ("/")
def index (was):
    html = (
        "<html>"
        "<header><title>Atila Example</title></header>"
        "<body>"
        "<h1><img src='{}' width=50 align='absmiddle'> Atila Example</h1>"
        "<div>This page is created by simple string, For viewing below link you need to install jinja2 using:</div>"
        "<blockquote>pip install jinja2</blockquote>"
        "<ul>"
        "<li><a href='{}'>View templated page example</a></li>"
        "</ul>"
        "</body>"
        "</html>"
    ).format (
        was.ab ("/avatar.png"),
        was.ab ('templates.index')
    )
    return html
