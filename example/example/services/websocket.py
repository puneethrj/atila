import skitai

def __mount__ (app):
    @app.route ("/echo")
    @app.websocket (skitai.WS_CHANNEL | skitai.WS_NOTHREAD)
    def echo (was, message):
        return "echo: " + message
