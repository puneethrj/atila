
def __mount__ (app):
    @app.route ('', methods = ["GET"])
    def index (was, message = 'Hellow, Atila'):
        return was.render ('index.jinja', your_message = message)

    @app.route ('/api-examples', methods = ["GET"])
    def api (was, message = 'Hellow, Atila'):
        return was.render ('api.jinja', your_message = message)
