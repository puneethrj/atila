import random, time, os

def background_job (wait = 2):
    time.sleep (wait)
    return [1, 2, 3]

def __mount__ (app):
    @app.route ('', methods = ["GET"])
    def index (was, message = ''):
        return was.API (
            your_message = message,
            referer = was.env.get ("HTTP_REFERER"),
            user_agent = was.env ["HTTP_USER_AGENT"]
        )

    @app.route ("/db")
    def db (was):
        with was.asyncon ("@sqlite3") as db:
            db.execute("create table if not exists test (name text, score int)").commit ()
            db.delete ('test').all ().execute ().commit ()

            for i in range (10):
                reqs = [
                    db.insert ('test').data (name = 'A', score = random.randrange (100)).execute (),
                    db.insert ('test').data (name = 'B', score = random.randrange (100)).execute ()
                ]
                was.Tasks (reqs).commit ()

            req = (db.select ('test')
                        .limit (10)
                        .filter (score__gt = 50)
                        .execute ())
        return was.API (rows = req.fetch ())

    @app.route ("/mixing")
    def mixing (was):
        def respond (was, tasks):
            a, b, c, d, e, f = tasks.fetch ()
            return was.API (a =a, b = b, c = c, d = d, e = e, f = f)
        return was.Tasks (
            was.db ("@sqlite3").execute ("select * from test"),
            was.get ("@pypi/project/rs4/", headers = [("Accept", "text/html")]),
            was.Thread (time.sleep, args = (0.3,)),
            was.Process (time.sleep, args = (0.3,)),
            was.Mask ('mask'),
            was.Subprocess ("ls"),
        ).then (respond)

    @app.route ("/mixing_taskmap")
    def mixing_taskmap (was):
        def sleep (timeout):
            time.sleep (timeout)
            return 1
        data = was.Tasks (
            was.Thread (time.sleep, args = (0.3,)),
            was.Process (time.sleep, args = (0.3,)),
            a = was.db ("@sqlite3").execute ("select * from test"),
            b = was.get ("@pypi/project/rs4/", headers = [("Accept", "text/html")]),
            e = was.Mask ('mask'),
            f = was.Subprocess ("ls")
        ).dict ()
        return was.API (data)

    @app.route ("/mixing_map")
    def mixing_map (was):
        def sleep (timeout):
            time.sleep (timeout)
            return 1
        return was.Map (
            was.Thread (time.sleep, args = (0.3,)),
            was.Process (time.sleep, args = (0.3,)),
            a = was.db ("@sqlite3").execute ("select * from test"),
            b = was.get ("@pypi/project/rs4/", headers = [("Accept", "text/html")]),
            e = was.Mask ('mask'),
            f = was.Subprocess ("ls")
        )

    @app.route ("/sp_map")
    def sp_map (was):
        return was.Map ('210 Success', a = was.Subprocess ("ls"))

    @app.route ("/sp_mapped")
    def sp_mapped (was):
        return was.Mapped ("210 Success", was.Tasks (a = was.Subprocess ("ls")))

    @app.route ("/th_map")
    def th_map (was):
        def environ ():
            return dict (os.environ)
        return was.Map ('210 Success', a = was.Thread (environ))

    @app.route ("/mixing_nested_taskmap")
    def mixing_nested_taskmap (was):
        def sleep (timeout):
            time.sleep (timeout)
            return 1

        nested = was.Tasks (
            was.Process (time.sleep, args = (0.3,)),
            e = was.Mask ('mask'),
            f = was.Subprocess ("ls")
        )
        data = was.Tasks (
            was.Thread (time.sleep, args = (0.3,)),
            a = was.db ("@sqlite3").execute ("select * from test"),
            b = was.get ("@pypi/project/rs4/", headers = [("Accept", "text/html")]),
            nested__dict = nested
        ).dict ()

        return was.API (data)

    @app.route ("/db2")
    def db2 (was):
        def respond (was, rs):
            return was.API (rows = rs.fetch ())

        with was.asyncon ("@sqlite3") as db:
            return (db.select ('test')
                        .limit (10)
                        .filter (score__gt = 50)
                        .execute ()).then (respond)

    @app.route ("/rest-api")
    def restapi (was):
        req = was.get ('@pypi/pypi/sampleproject/json')
        return was.API (
            result = req.fetch ()
        )

    @app.route ("/rest-api2")
    def restapi2 (was):
        def respond (was, rs):
            return was.API (result = rs.fetch ())
        return was.get ('@pypi/pypi/sampleproject/json').then (respond)

    @app.route ("/xmlrpc")
    def xmlrpc (was):
        with was.xmlrpc ("@pypi/pypi") as stub:
            req = stub.package_releases ('roundup')
            return was.API (
                method_name = 'package_releases',
                result = req.fetch ()
            )

    @app.route ("/thread")
    def thread (was):
        s = time.time ()
        req = was.Thread (background_job, 2)
        time.sleep (2)
        return was.API (
            duration = time.time () - s,
            result = req.fetch ()
        )

    @app.route ("/thread2")
    def thread2 (was):
        def respond (was, rs):
            return was.API (result = rs.fetch ())
        return was.Thread (background_job, 2).then (respond)

    @app.route ("/process")
    def process (was):
        s = time.time ()
        req = was.Process (background_job, 2)
        time.sleep (2)
        return was.API (
            duration = time.time () - s,
            result = req.fetch ()
        )

    @app.route ("/subprocess")
    def subprocess (was):
        req = was.Subprocess ("ls -lf")
        return was.API (
            result = req.fetch ().split ()
        )

    @app.route ("/urlfor")
    def urlfor (was):
        return was.API (
            urls = [
                was.ab (index, 'urlfor'),
                was.ab (index),
                was.ab (db),
                was.ab ('templates.index'),
                was.ab ('templates.index', 'urlfor'),
                was.ab ('templates.api')
            ]
        )

    @app.route ("/map")
    def map (was):
        return was.Map (
            was.db ("@sqlite3").execute ("select * from people"),
            was.get ("@pypi/project/rs4/", headers = [("Accept", "text/html")]),
            a = 123,
            b = '456',
            c = was.Thread (time.sleep, args = (0.3,)),
            d = was.Process (time.sleep, args = (0.3,)),
            e = was.Mask ('mask'),
            f = was.Subprocess ("ls")
        )